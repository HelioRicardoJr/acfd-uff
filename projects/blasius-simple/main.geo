//Inputs
convertToMeters = 0.1;
boxdim_x = 20;
boxdim_y = 1; 
celldim1 = 100;
celldim2 = 20;
depth    = boxdim_y*convertToMeters/10;
f_refine = 1.2;

Point(1) = {0,0,0};
Point(2) = {boxdim_x*convertToMeters,0,0};
Point(3) = {boxdim_x*convertToMeters,boxdim_y*convertToMeters,0};
Point(4) = {0,boxdim_y*convertToMeters,0};

Line(5) = {1,2};
Line(6) = {2,3};
Line(7) = {3,4};
Line(8) = {4,1};

Line Loop(9) = {5,6,7,8};
Plane Surface(10) = 9;

Transfinite Line{5,6,7,8} = celldim1+1;
Transfinite Line{6,-8} = celldim2+1 Using Progression f_refine;

Transfinite Surface{10};
Recombine Surface{10};

newEntities[] = 
Extrude{0,0,depth}
{
	Surface{10};
	Layers{1};
	Recombine;
};

Physical Surface("inlet") = {newEntities[{5}]};
Physical Surface("outlet") = {newEntities[{3}]};
Physical Surface("symmetryPlane") = {newEntities[4]};
Physical Surface("bottomWall") = {newEntities[2]};
Physical Surface("frontAndBack") = {10,newEntities[0]};
Physical Volume(100) = {newEntities[1]};
