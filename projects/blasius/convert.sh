#!/bin/bash

gmsh main.geo -3 -o test.msh
gmshToFoam test.msh -case cavity
# cp -v cavity/boundary cavity/constant/polyMesh
rm -v test.msh *~
