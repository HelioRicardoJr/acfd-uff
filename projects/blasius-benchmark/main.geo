//Inputs
boxdim_x = 1.0e-2; // 1 cm
boxdim_y = 4.0e-3; // 4mm
celldim  = 20;
depth    = boxdim_y/100;
f_refine = 1.0;

Point(1) = {boxdim_x,0,0};
Point(2) = {4*boxdim_x,0,0};
Point(3) = {4*boxdim_x,boxdim_y,0};
Point(4) = {boxdim_x,boxdim_y,0};

Line(5) = {1,2};
Line(6) = {2,3};
Line(7) = {3,4};
Line(8) = {4,1};

Line Loop(9) = {5,6,7,8};
Plane Surface(10) = 9;

Transfinite Line{5,6,7,8} = celldim+1;
Transfinite Line{6,-8} = celldim+1 Using Progression f_refine;

Transfinite Surface{10};
Recombine Surface{10};

newEntities[] = 
Extrude{0,0,depth}
{
	Surface{10};
	Layers{1};
	Recombine;
};

Physical Surface("Inlet") = {newEntities[{5}]};
Physical Surface("Outlet") = {newEntities[{3}]};
Physical Surface("upWall") = {newEntities[4]};
Physical Surface("downWall") = {newEntities[2]};
Physical Surface("frontAndBack") = {10,newEntities[0]};
Physical Volume(100) = {newEntities[1]};
